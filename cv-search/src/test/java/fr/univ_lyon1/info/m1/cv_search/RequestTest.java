package fr.univ_lyon1.info.m1.cv_search;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;

import fr.univ_lyon1.info.m1.cv_search.model.Model;
import fr.univ_lyon1.info.m1.cv_search.model.request.Request;

import java.util.ArrayList;
import java.util.List;

public class RequestTest {

    //test avec requete par défaut
    @Test
    public void testRequestEmpty() {
        // Given
        List<String> searchSkills = new ArrayList<String>();
        List<String> searchLanguage = new ArrayList<String>();
        Model m = new Model();
        m.init();

        // When
        List<String> selectedApplicants = Request.getInstance()
            .makeRequest(m, searchSkills, searchLanguage);

        // Then
        assertThat(5, is(selectedApplicants.size()));
        assertThat(true, is(selectedApplicants.contains("Foo Bar")));
        assertThat(true, is(selectedApplicants.contains("John Smith")));
    }

    //test pour le total experience time
    @Test
    public void testRequestTotalExperienceTime() {
        // Given
        List<String> searchSkills = new ArrayList<String>();
        List<String> searchLanguage = new ArrayList<String>();
        Model m = new Model();
        m.init();
        m.setTotalExperienceTime(10);

        // When
        List<String> selectedApplicants = Request.getInstance().makeRequest(
            m, searchSkills, searchLanguage);
        m.setTotalExperienceTime(20);
        List<String> selectedApplicantsMax = Request.getInstance().makeRequest(
            m, searchSkills, searchLanguage);

        // Then
        assertThat(2, is(selectedApplicants.size()));
        assertThat(0, is(selectedApplicantsMax.size()));
    }

    //tests pour le skill level
    @Test
    public void testRequestSkillLevelWithNoSkills() {
        // Given
        List<String> searchSkills = new ArrayList<String>();
        List<String> searchLanguage = new ArrayList<String>();
        Model m = new Model();
        m.init();

        List<String> selectedApplicants = Request.getInstance().makeRequest(
                m, searchSkills, searchLanguage);

        String[] strategies = {"All >= 50", "All >= 60", "Average >= 50"};
        for (int i=0; i<2; i++){
            // When
            m.setSkillLevel(strategies[i]);
            List<String> selectedApplicantsSkillLevel = Request.getInstance().makeRequest(
                m, searchSkills, searchLanguage);

            // Then
            assertThat(selectedApplicants, is(selectedApplicantsSkillLevel));
        }
    }

    @Test
    public void testRequestSkillLevelWithUnknownSkills() {
        // Given
        List<String> searchSkills = new ArrayList<String>();
        searchSkills.add("cc");
        List<String> searchLanguage = new ArrayList<String>();
        Model m = new Model();
        m.init();

        String[] strategies = {"All >= 50", "All >= 60", "Average >= 50"};
        for (int i=0; i<2; i++){
            // When
            m.setSkillLevel(strategies[i]);
            List<String> selectedApplicants = Request.getInstance().makeRequest(
                m, searchSkills, searchLanguage);

            // Then
            assertThat(0, is(selectedApplicants.size()));
        }
    }

    @Test
    public void testRequestSkillLevelAllSup() {
        // Given
        List<String> searchSkills = new ArrayList<String>();
        searchSkills.add("java");
        List<String> searchLanguage = new ArrayList<String>();
        Model m = new Model();
        m.init();

        // When
        m.setSkillLevel("All >= 50");
        List<String> selectedApplicants50 = Request.getInstance().makeRequest(
            m, searchSkills, searchLanguage);
        m.setSkillLevel("All >= 60");
        List<String> selectedApplicants60 = Request.getInstance().makeRequest(
            m, searchSkills, searchLanguage);

        // Then
        assertThat(2, is(selectedApplicants50.size()));
        assertThat("Foo Bar", is(selectedApplicants50.get(0)));
        assertThat("John Smith", is(selectedApplicants50.get(1)));
        assertThat(0, is(selectedApplicants60.size()));
    }

    @Test
    public void testRequestSkillLevelAverageSup() {
        // Given
        List<String> searchSkills1 = new ArrayList<String>();
        searchSkills1.add("c");
        List<String> searchSkills2 = new ArrayList<String>();
        searchSkills2.add("c");
        searchSkills2.add("c++");
        List<String> searchLanguage = new ArrayList<String>();
        Model m = new Model();
        m.init();
        m.setSkillLevel("Average >= 50");

        // When
        List<String> selectedApplicants1 = Request.getInstance().makeRequest(
        m, searchSkills1, searchLanguage);
        List<String> selectedApplicants2 = Request.getInstance().makeRequest(
        m, searchSkills2, searchLanguage);

        // Then
        assertThat(1, is(selectedApplicants1.size()));
        assertThat("John Smith", is(selectedApplicants1.get(0)));

        assertThat(2, is(selectedApplicants2.size()));
        assertThat("Foo Bar", is(selectedApplicants2.get(0)));
        assertThat("John Smith", is(selectedApplicants2.get(1)));
    }

    //Tests pour le required experience time 
    @Test
    public void testRequestExperienceTimeWithNoSkills() {
        // Given
        List<String> searchSkills = new ArrayList<String>();
        List<String> searchLanguage = new ArrayList<String>();
        Model m = new Model();
        m.init();

        List<String> selectedApplicants = Request.getInstance().makeRequest(
            m, searchSkills, searchLanguage);

        String[] expTime = {"All >= 1", "All >= 3", "All >= 5", "All >= 10", "Average >= 10"};
        for (int i=0; i<5; i++){
            // When
            m.setExpTimeRequired(expTime[i]);
            List<String> selectedApplicants2 = Request.getInstance().makeRequest(
                m, searchSkills, searchLanguage);

            // Then
            assertThat(selectedApplicants, is(selectedApplicants2));
        }
    }

    @Test
    public void testRequestExperienceTimeWithUnknownSkills() {
        // Given
        List<String> searchSkills = new ArrayList<String>();
        searchSkills.add("cc");
        List<String> searchLanguage = new ArrayList<String>();
        Model m = new Model();
        m.init();

        String[] expTime = {"All >= 1", "All >= 3", "All >= 5", "All >= 10", "Average >= 10"};
        for (int i=0; i<5; i++){
            // When
            m.setExpTimeRequired(expTime[i]);
            List<String> selectedApplicants = Request.getInstance().makeRequest(
                m, searchSkills, searchLanguage);

            // Then
            assertThat(0, is(selectedApplicants.size()));
        }
    }

    @Test
    public void testRequestExperienceTimeAllSup() {
        // Given
        List<String> searchSkills = new ArrayList<String>();
        searchSkills.add("java");
        List<String> searchLanguage = new ArrayList<String>();
        Model m = new Model();
        m.init();

        int[] nbResults= {2, 2, 2, 1};
        String[] expTime = {"All >= 1", "All >= 3", "All >= 5", "All >= 10"};
        for (int i=0; i<4; i++){
            // When
            m.setExpTimeRequired(expTime[i]);
            List<String> selectedApplicants = Request.getInstance().makeRequest(
                m, searchSkills, searchLanguage);

            // Then
            assertThat(nbResults[i], is(selectedApplicants.size()));
        }
    }

    @Test
    public void testRequestExperienceTimeAverageSup() {
        // Given
        List<String> searchSkills1 = new ArrayList<String>();
        searchSkills1.add("c");
        List<String> searchSkills2 = new ArrayList<String>();
        searchSkills2.add("c");
        searchSkills2.add("python");
        List<String> searchLanguage = new ArrayList<String>();
        Model m = new Model();
        m.init();
        m.setExpTimeRequired("Average >= 10");

        // When
        List<String> selectedApplicants1 = Request.getInstance().makeRequest(
            m, searchSkills1, searchLanguage);
        List<String> selectedApplicants2 = Request.getInstance().makeRequest(
            m, searchSkills2, searchLanguage);

        // Then
        assertThat(1, is(selectedApplicants1.size()));
        assertThat("Foo Bar", is(selectedApplicants1.get(0)));

        assertThat(0, is(selectedApplicants2.size()));
    }

    //tests pour SortBy
    @Test
    public void testRequestSortByAlphabetical() {
        // Given
        List<String> searchSkills = new ArrayList<String>();
        List<String> searchLanguage = new ArrayList<String>();
        Model m = new Model();
        m.init();

        // When
        List<String> selectedApplicants = Request.getInstance().makeRequest(
            m, searchSkills, searchLanguage);

        // Then
        assertThat("Bip Bip", is(selectedApplicants.get(0))); 
    }

    //tests pour showSkillAverage
    @Test
    public void testRequestShowSkillAverageNoSkill() {
        // Given
        List<String> searchSkills = new ArrayList<String>();
        List<String> searchLanguage = new ArrayList<String>();
        Model m = new Model();
        m.init();
        m.setShowSkillsAverage(true);

        // When
        List<String> selectedApplicants = Request.getInstance().makeRequest(
            m, searchSkills, searchLanguage);

        // Then
        assertThat(true, is(selectedApplicants.get(0).endsWith("None")));
    }

    @Test
    public void testRequestShowSkillAverageUnknownSkill() {
        // Given
        List<String> searchSkills = new ArrayList<String>();
        searchSkills.add("cc");
        List<String> searchLanguage = new ArrayList<String>();
        Model m = new Model();
        m.init();
        m.setShowSkillsAverage(true);

        // When
        List<String> selectedApplicants = Request.getInstance().makeRequest(
            m, searchSkills, searchLanguage);

        // Then
        assertThat(0, is(selectedApplicants.size()));
    }

    @Test
    public void testRequestShowSkillAverageKnownSkills() {
        // Given
        List<String> searchSkills = new ArrayList<String>();
        searchSkills.add("c++");
        searchSkills.add("c");
        List<String> searchLanguage = new ArrayList<String>();
        Model m = new Model();
        m.init();

        // When
        List<String> selectedApplicantsTemoin = Request.getInstance().makeRequest(
            m, searchSkills, searchLanguage);
        m.setShowSkillsAverage(true);
        List<String> selectedApplicants = Request.getInstance().makeRequest(
            m, searchSkills, searchLanguage);

        // Then
        assertThat("Foo Bar", is(selectedApplicantsTemoin.get(0)));
        assertThat(true, is(selectedApplicants.get(0).endsWith("55.0")));
        assertThat("Jack Dupont", is(selectedApplicantsTemoin.get(1)));
        assertThat(true, is(selectedApplicants.get(1).endsWith("12.0")));
        assertThat("John Smith", is(selectedApplicantsTemoin.get(2)));
        assertThat(true, is(selectedApplicants.get(2).endsWith("80.0")));
    }

    //tests pour language level
    @Test
    public void testRequestLanguageLevel() {
        // Given
        List<String> searchSkills = new ArrayList<String>();
        List<String> searchLanguage = new ArrayList<String>();
        searchLanguage.add("English");
        Model m = new Model();
        m.init();

        // When
        m.setLanguageLevel("All >= A1");
        List<String> selectedApplicants = Request.getInstance().makeRequest(
            m, searchSkills, searchLanguage);
        m.setLanguageLevel("All >= C2");
        List<String> selectedApplicants2 = Request.getInstance().makeRequest(
            m, searchSkills, searchLanguage);

        // Then
        assertThat(3, is(selectedApplicants.size()));
        assertThat("Jack Dupont", is(selectedApplicants.get(1)));
        assertThat(2, is(selectedApplicants2.size()));
        assertThat("Foo Bar", is(selectedApplicants2.get(0)));
        assertThat("John Smith", is(selectedApplicants2.get(1)));
    }

        //tests pour study level
        @Test
        public void testRequestStudyLevel() {
            // Given
            List<String> searchSkills = new ArrayList<String>();
            List<String> searchLanguage = new ArrayList<String>();
            Model m = new Model();
            m.init();
    
            // When
            m.setStudyLevel("Bac");
            List<String> selectedApplicants = Request.getInstance().makeRequest(
                m, searchSkills, searchLanguage);
            m.setStudyLevel("Bac+3");
            List<String> selectedApplicants2 = Request.getInstance().makeRequest(
                m, searchSkills, searchLanguage);
            m.setStudyLevel("Bac+5");
            List<String> selectedApplicants3 = Request.getInstance().makeRequest(
            m, searchSkills, searchLanguage);
            m.setStudyLevel("Bac+8");
            List<String> selectedApplicants4 = Request.getInstance().makeRequest(
                m, searchSkills, searchLanguage);
    
            // Then
            assertThat(4, is(selectedApplicants.size()));
            assertThat(2, is(selectedApplicants2.size()));
            assertThat("Foo Bar", is(selectedApplicants2.get(0)));
            assertThat("John Smith", is(selectedApplicants2.get(1)));
            assertThat(1, is(selectedApplicants3.size()));
            assertThat("John Smith", is(selectedApplicants3.get(0)));
            assertThat(0, is(selectedApplicants4.size()));
        }
    
}
