package fr.univ_lyon1.info.m1.cv_search;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;
//import fr.univ_lyon1.info.m1.cv_search.model.request.Request;
import fr.univ_lyon1.info.m1.cv_search.observer.ObserverExpTime;
import fr.univ_lyon1.info.m1.cv_search.observer.ObserverLanguages;
//import fr.univ_lyon1.info.m1.cv_search.observer.ObserverRequest;
import fr.univ_lyon1.info.m1.cv_search.observer.ObserverShowAverage;
import fr.univ_lyon1.info.m1.cv_search.observer.ObserverSkill;
import fr.univ_lyon1.info.m1.cv_search.observer.ObserverSortBy;
import fr.univ_lyon1.info.m1.cv_search.observer.ObserverSkillLevel;
import fr.univ_lyon1.info.m1.cv_search.observer.ObserverStudyLevel;
import fr.univ_lyon1.info.m1.cv_search.observer.ObserverTotalExpTime;
//import fr.univ_lyon1.info.m1.cv_search.view.ComboBoxExpTime;
//import fr.univ_lyon1.info.m1.cv_search.view.JfxView;
//import javafx.stage.Stage;

public class ObserverTest {
    @Test
    public void testObserverExpTime() { 
        // Given
        Model model = new Model();
        Controller c = new Controller(); 
        //JfxView v = new JfxView(c, new Stage(), 600, 600, model);
        //c.addView(v);
        ObserverExpTime observer = new ObserverExpTime(model, c);

        // When
        observer.update("All >= 1");

        // Then
        //ComboBoxExpTime box = (ComboBoxExpTime)v.getComboBoxExpTime();
        assertThat("All >= 1", is(model.getExpTimeRequired()));
        //assertThat("All >= 60", is((box.getBox()).getValue()));
    }

    @Test
    public void testObserverLanguages() { 
        // Given
        Model model = new Model();
        Controller c = new Controller(); 
        ObserverLanguages observer = new ObserverLanguages(model, c);

        // When
        observer.update("English");

        // Then
        assertThat("English", is(model.getLastLanguage()));
    }

    @Test
    public void testObserverRequest() { 
        // Given
        //Model model = new Model();
        //Controller c = new Controller(); 
        //ObserverRequest observer = new ObserverRequest(model, c);
        //List<String> selectedApplicants = Request.getInstance().makeRequest(
        //    sortBy, "", "", searchSkills, false, 10);

        // When
        //observer.update(selectedApplicants);

        // Then
        //assertThat(selectedApplicants, is(model.getSelectedApplicants()));
    }

    @Test
    public void testObserverShowSkillAverage() { 
        // Given
        Model model = new Model();
        Controller c = new Controller(); 
        ObserverShowAverage observer = new ObserverShowAverage(model, c);

        // When
        observer.update(false);

        // Then
        assertThat(false, is(model.getShowSkillsAverage()));
    }

    @Test
    public void testObserverSkill() { 
        // Given
        Model model = new Model();
        Controller c = new Controller(); 
        ObserverSkill observer = new ObserverSkill(model, c);

        // When
        observer.update("c++");
        observer.update("c");
        observer.update("c");
        List<String> skills = model.getSkillList();
        

        // Then
        assertThat(true, is(skills.contains("c++")));
        assertThat(false, is(skills.contains("c")));
    }

    @Test
    public void testObserverSortBy() { 
        // Given
        Model model = new Model();
        Controller c = new Controller(); 
        ObserverSortBy observer = new ObserverSortBy(model, c);

        // When
        observer.update("alphabetical");

        // Then
        assertThat("alphabetical", is(model.getSortBy()));
    }
   
    @Test
    public void testObserverStrategy() { 
        // Given
        Model model = new Model();
        Controller c = new Controller(); 
       // c.addView(new JfxView(c, new Stage(), 600, 600, model));
        ObserverSkillLevel observer = new ObserverSkillLevel(model, c);

        // When
        observer.update("All >= 60");

        // Then
        //JfxView v = c.getView();
        assertThat("All >= 60", is(model.getSkillLevel()));
        //assertThat("All >= 60", is((ComboxSkillLevel)v.getComboBoxSkillLevel().getValue()));
    }

    @Test
    public void testStudyLevel() { 
        // Given
        Model model = new Model();
        Controller c = new Controller(); 
        ObserverStudyLevel observer = new ObserverStudyLevel(model, c);

        // When
        observer.update("Bac+2");

        // Then
        assertThat("Bac+2", is(model.getStudyLevel()));
    }
 
    @Test
    public void testTotalExp() { 
        // Given
        Model model = new Model();
        Controller c = new Controller(); 
        ObserverTotalExpTime observer = new ObserverTotalExpTime(model, c);

        // When
        observer.update(5);

        // Then
        assertThat(5, is(model.getTotalExperienceTime()));
    }

}
