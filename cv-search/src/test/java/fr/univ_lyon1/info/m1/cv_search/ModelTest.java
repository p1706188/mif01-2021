package fr.univ_lyon1.info.m1.cv_search;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.Test;

import fr.univ_lyon1.info.m1.cv_search.model.Model;

public class ModelTest {
    
    /** Test of init and setters (est ce qu'il faut faire les setters a cote ? ) */
    @Test
    public void testInit() {
        // Given
        //Model model = new Model();

        // When
        //model.init();

        // Then
        //assertThat("", is(model.getStrategyName()));
        //assertThat("", is(model.getExpTimeRequired()));
        //assertThat("alphabetical", is(model.getSortBy()));
        //assertThat("", is(model.getStudyLevel()));
        //assertThat(false, is(model.getShowSkillsAverage()));
        //assertThat(0, is(model.getTotalExperienceTime()));
    }

    @Test 
    public void testAddSkill(){
        //Given
        Model model = new Model(); //(init ?)

        //When 
        String VR = model.changeSkillList("c++");

        //Then 
        List<String> skillList = model.getSkillList();
        assertThat(true, is(skillList.contains("c++")));
        assertThat("", is(VR));
    }

    @Test 
    public void testDeleteSkill(){
        //Given
        Model model = new Model(); //(init ?)
        model.changeSkillList("c++"); //add skill "c++"

        //When 
        String VR = model.changeSkillList("c++");

        //Then 
        List<String> skillList = model.getSkillList();
        assertThat(false, is(skillList.contains("c++")));
        assertThat("c++", is(VR));
    }

    @Test 
    public void testAddLanguage(){
        //Given
        Model model = new Model(); //(init ?)

        //When 
        String VR = model.changeLanguages("English");

        //Then 
        List<String> languageList = model.getLanguageList();
        assertThat(true, is(languageList.contains("English")));
        assertThat("", is(VR));
    }

    @Test 
    public void testDeletelanguage(){
        //Given
        Model model = new Model(); //(init ?)
        model.changeLanguages("English"); //add skill "English"

        //When 
        String VR = model.changeLanguages("English");

        //Then 
        List<String> languageList = model.getLanguageList();
        assertThat(false, is(languageList.contains("English")));
        assertThat("English", is(VR));
    }
}
