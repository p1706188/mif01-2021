package fr.univ_lyon1.info.m1.cv_search;

import java.io.File;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;

import fr.univ_lyon1.info.m1.cv_search.model.applicant.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.applicant.ApplicantBuilder;
import fr.univ_lyon1.info.m1.cv_search.model.applicant.ApplicantList;
import fr.univ_lyon1.info.m1.cv_search.model.applicant.ApplicantListBuilder;

import java.util.ArrayList;
import java.util.List;

public class ApplicantTest {

    @Test
    public void testReadApplicant() {
        // Given
        ApplicantBuilder builder = new ApplicantBuilder("applicant1.yaml");
        List<String> searchSkills = new ArrayList<String>();
        searchSkills.add("c");
        searchSkills.add("c++");
        searchSkills.add("java");

        // When
        Applicant a = builder.build();

        // Then
        assertThat(70, is(a.getSkill("c++")));
        assertThat("John Smith", is(a.getName()));
        assertThat(16, is(a.getTotalExperienceTime()));
        assertThat(5, is(a.getExperienceTime("c++")));
        assertThat(70.0F, is(a.calculateAverage(searchSkills)));
        assertThat(5.0F, is(a.calculateAverageTime(searchSkills)));
    }

    @Test
    public void testReadApplicantEmptySearchSkill() {
        // Given
        ApplicantBuilder builder = new ApplicantBuilder("applicant1.yaml");
        List<String> searchSkills = new ArrayList<String>();

        // When
        Applicant a = builder.build();

        // Then
        assertThat(101.0F, is(a.calculateAverage(searchSkills)));
        assertThat(101.0F, is(a.calculateAverageTime(searchSkills)));
    }

    @Test
    public void testReadApplicantSearchWithNonExistantSkill() {
        // Given
        ApplicantBuilder builder = new ApplicantBuilder("applicant1.yaml");
        List<String> searchSkills = new ArrayList<String>();
        searchSkills.add("ccc");

        // When
        Applicant a = builder.build();

        // Then
        assertThat(-1, is(a.getSkill("ccc")));
        assertThat(0, is(a.getExperienceTime("ccc")));
        assertThat(-1.0F, is(a.calculateAverage(searchSkills)));
        assertThat(0.0F, is(a.calculateAverageTime(searchSkills)));
    }

    @Test
    public void testReadApplicantWithNoSkillAndNoExperience() {
        // Given
        ApplicantBuilder builder = new ApplicantBuilder("applicant5.yaml");
        List<String> searchSkills = new ArrayList<String>();

        // When
        Applicant a = builder.build();

        // Then
        assertThat(-1, is(a.getSkill("c++")));
        assertThat(0, is(a.getExperienceTime("c++")));
        assertThat(101.0F, is(a.calculateAverage(searchSkills)));
        assertThat(101.0F, is(a.calculateAverageTime(searchSkills)));
        assertThat(0, is(a.getTotalExperienceTime()));
    }

    @Test
    public void testReadManyApplicant() {
        // Given
        ApplicantListBuilder builder = new ApplicantListBuilder(new File("."));

        // When
        ApplicantList list = builder.build();

        // Then
        boolean johnFound = false;
        for (Applicant a : list) {
            if (a.getName().equals("John Smith")) {
                assertThat(90, is(a.getSkill("c")));
                assertThat(70, is(a.getSkill("c++")));
                johnFound = true;
            }
        }
        assertThat(johnFound, is(true));
    }
}
