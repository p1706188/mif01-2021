package fr.univ_lyon1.info.m1.cv_search.observer;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;

public class ObserverSkillLevel extends Observer {
    /** constructor. */
    public ObserverSkillLevel(Model model, Controller c) {
        super(model, c);
    }

    /** surcharge update. */
    public void update(Object skillLevelName) {
        getModel().setSkillLevel((String) skillLevelName);
        getController().updateView(2, "");
    }
}
