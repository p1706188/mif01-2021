package fr.univ_lyon1.info.m1.cv_search.model.request;

import java.util.List;

import fr.univ_lyon1.info.m1.cv_search.model.applicant.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.applicant.ApplicantList;

import java.util.ArrayList;

public abstract class Filter extends ApplicantFilter {

    private int n = 0; //nombre dans la stratégie (par ex : "All > n")
    private List<String> searchedThings = new ArrayList<String>();
    

    /** strategy setter (n and searchedThings). */
    public void setFilterAttributes(int n, List<String> searchedThings) {
        this.n = n;
        this.searchedThings = searchedThings;
    }
    
    /** Takes a list of things and a strategy. 
     * Returns a list of results (applicant name and average note). */
    public ApplicantList filter(ApplicantList listApplicants, String filterName) {
        if (searchedThings.isEmpty() 
            && filterName != "totalExpTime" && filterName != "studyLevel") { 
            return listApplicants; //si il y a pas de things, la liste change pas
        }
        ApplicantList selectedApplicantList = new ApplicantList();
        for (Applicant a : listApplicants) {
            boolean selected = true;
            selected = isSelected(a, filterName);
            if (selected) {
                selectedApplicantList.add(a);
            }
        }
        return selectedApplicantList;
    }

    /** return true if applicant is selected. */
    public abstract boolean isSelected(Applicant a, String filterName);

    /** n getter. */
    int getN() {
        return n;
    }

    /** searchedThings getter.  */
    List<String> getSearchedThings() {
      return searchedThings;
    }
}   
