package fr.univ_lyon1.info.m1.cv_search.observer;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;

public class ObserverStudyLevel extends Observer {
    /** constructor. */
    public ObserverStudyLevel(Model model, Controller c) {
        super(model, c);
    }

    /** surchage update. */
    public void update(Object studyLevel) {
        getModel().setStudyLevel((String) studyLevel);
        getController().updateView(10, "");
    }
}
