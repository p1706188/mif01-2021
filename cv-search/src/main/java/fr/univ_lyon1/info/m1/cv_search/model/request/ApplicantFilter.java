package fr.univ_lyon1.info.m1.cv_search.model.request;

import fr.univ_lyon1.info.m1.cv_search.model.applicant.ApplicantList;

public abstract class ApplicantFilter {

    public abstract ApplicantList filter(ApplicantList listApplicants, String filterName);
}
