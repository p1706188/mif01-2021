package fr.univ_lyon1.info.m1.cv_search.view;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;
import fr.univ_lyon1.info.m1.cv_search.observer.Observer;

public class JfxViewComponent {
    private Observer observer;
    private Model model;
    private Controller c;

    public JfxViewComponent(Model m, Controller c) {
        this.model = m;
        this.c = c;
    }

    /** Notify observer. */
    public void notifyObservers(Object object) {
        observer.update(object);
    }

    /** Model getter. */
    Model getModel() {
        return model;
    }

    /** Controller getter. */
    Controller getController() {
        return c;
    }

    /** Controller setter. */
    void setController(Controller cc) {
        c = cc;
    }

    /** Observer setter. */
    void setObserver(Observer o) {
        observer = o;
    }
}
