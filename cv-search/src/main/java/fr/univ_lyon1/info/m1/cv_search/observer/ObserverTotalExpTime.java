package fr.univ_lyon1.info.m1.cv_search.observer;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;

public class ObserverTotalExpTime extends Observer {
    /** Constructeur. */
    public ObserverTotalExpTime(Model model, Controller c) {
        super(model, c);
    }

    /** sucharge update. */
    public void update(Object totalExperienceTime) {
        getModel().setTotalExperienceTime((int) totalExperienceTime);
        getController().updateView(6, "");
    }
}
