package fr.univ_lyon1.info.m1.cv_search.model.applicant;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.List;

import org.yaml.snakeyaml.Yaml;

public class ApplicantBuilder {

    private File file;

    public ApplicantBuilder(File f) {
        this.file = f;
    }

    public ApplicantBuilder(String filename) {
        this.file = new File(filename);
    }

    /**
     * Build the applicant from the Yaml file provided to the constructor.
     */
    public Applicant build() {
        Applicant a = new Applicant();
        Yaml yaml = new Yaml();
        Map<String, Object> map;
        try {
            map = yaml.load(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new Error(e);
        }

        a.setName((String) map.get("name"));

        // Cast may fail if the Yaml is incorrect. Ideally we should provide
        // clean error messages.
        @SuppressWarnings("unchecked")
        Map<String, Integer> skills = (Map<String, Integer>) map.get("skills");

        if (skills != null) {
            for (String skill : skills.keySet()) {
                Integer value = skills.get(skill);
                a.setSkill(skill, value);
            }
        }
        

        int totalExperienceTime = 0;

        //pour l'experience pro
        @SuppressWarnings("unchecked")
        Map<String, Object> mapExperiences = ((Map<String, Object>) map.get("experience"));
        
        if (mapExperiences != null) {
            for (String expName : mapExperiences.keySet()) {
                @SuppressWarnings("unchecked")
                Map<String, Object> mapExperience = 
                    (Map<String, Object>) mapExperiences.get(expName);
                
                Integer start = (Integer) mapExperience.get("start");
                Integer end = (Integer) mapExperience.get("end");
                int time = end - start;
                totalExperienceTime += time;

                @SuppressWarnings("unchecked")
                List<String> mapKeywords = (List<String>) mapExperience.get("keywords");
                for (int i = 0; i < mapKeywords.size(); i++) {
                    String skill = mapKeywords.get(i);
                    int experienceTime = a.getExperienceTime(skill); //O si la key existe pas
                    a.setExperienceTime(skill, experienceTime + time);
                }
            }
        }

        a.setTotalExperienceTime(totalExperienceTime);

        @SuppressWarnings("unchecked")
        Map<String, String> languages = (Map<String, String>) map.get("languages");

        if (skills != null) {
            for (String language : languages.keySet()) {
                String value = languages.get(language);
                a.setLanguage(language, value);
            }
        }

        a.setStudyLevel((String) map.get("studyLevel"));

        return a;
    }
}
