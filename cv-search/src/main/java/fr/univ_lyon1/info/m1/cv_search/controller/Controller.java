package fr.univ_lyon1.info.m1.cv_search.controller;

import fr.univ_lyon1.info.m1.cv_search.model.Model;
import fr.univ_lyon1.info.m1.cv_search.model.request.Request;
import fr.univ_lyon1.info.m1.cv_search.view.JfxView;

import java.util.List;
import java.util.ArrayList;

public class Controller {

    private List<JfxView> viewsList = new ArrayList<>(); 

    /** calls requests to get results. */
    public List<String> askSearchResults(Model m, 
        List<String> searchSkills, List<String> searchLang) {

        List<String> selectedApplicants = 
            Request.getInstance().makeRequest(m, searchSkills, searchLang);
        return selectedApplicants;
    }

    /** Add views. */
    public void addViews(List<JfxView> viewList) {
        for (int i = 0; i < viewList.size(); i++) {
            this.viewsList.add(viewList.get(i));
        }
    }

    public void addView(JfxView view) {
        this.viewsList.add(view);
    }

    /** Update views after call of Observers update . */
    public void updateView(int i, String str) {
        for (int j = 0; j < viewsList.size(); j++) {
            switch (i) {
                case 1:
                    if (str == "") {
                        (viewsList.get(j)).getSkillBox().updateSkills();
                    } else {
                        (viewsList.get(j)).getSkillBox().updateRemoveSkill(str);
                    }
                    break;
                case 2:
                    (viewsList.get(j)).getComboBoxSkillLevel().updateModel();
                    break;
                case 3:
                    (viewsList.get(j)).getShowAverageButton().updateShowAverage();
                    break;
                case 4:
                    (viewsList.get(j)).getComboBoxExpTime().updateModel();
                    break;
                case 5:
                    (viewsList.get(j)).getSearchWidget().updateResultBox();
                    break;
                case 6:
                    (viewsList.get(j)).getTotalExperienceTimeBox().updateTotExpTime();
                    break;
                case 7 :
                    (viewsList.get(j)).getComboBoxSortBy().updateModel();
                    break;
                case 8 :
                    if (str == "") {
                        (viewsList.get(j)).getComboBoxLanguage().updateLanguages();
                    } else {
                        (viewsList.get(j)).getComboBoxLanguage().updateRemoveLanguages(str);
                    }
                    break;
                case 9 : 
                    (viewsList.get(j)).getComboBoxLanguageLevel().updateModel();
                    break;
                case 10 :
                    (viewsList.get(j)).getComboBoxStudyLevel().updateModel();
                    break;
                case 11 :
                    (viewsList.get(j)).getComboBoxLanguage().updateSelectedLanguage(str);
                    break;
                default:
                    break;
            } 
        }
    } 
}
