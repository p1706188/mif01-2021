package fr.univ_lyon1.info.m1.cv_search.model.request;

import java.util.List;

import fr.univ_lyon1.info.m1.cv_search.model.applicant.Applicant;

public class FilterOther extends Filter {
    /** implementation of isSelected. 
     * Returns true if the applicant is selected through the filter. */
    public boolean isSelected(Applicant a, String filterName) {
        boolean selected = true;
        float thingToCompare = 101;
        List<String> searchedThings = getSearchedThings();

        if (filterName == "skillLevel") {
            thingToCompare = a.calculateAverage(searchedThings);
        } else if (filterName == "ExperienceTime") {
            thingToCompare = a.calculateAverageTime(searchedThings);
        } else if (filterName == "totalExpTime") {
            thingToCompare = a.getTotalExperienceTime(); 
        } else if (filterName == "studyLevel") {
            thingToCompare = a.getStudyLevel(); 
        }

        if (thingToCompare < getN()) {
            selected = false;
        }
        return selected;
    }
}
