package fr.univ_lyon1.info.m1.cv_search.model.request;

import java.util.List;

import fr.univ_lyon1.info.m1.cv_search.model.applicant.Applicant;

public class FilterAllSuperior extends Filter {
    /** implementation of isSelected. 
     * Returns true if the applicant is selected through the filter. */
    public boolean isSelected(Applicant a, String filterName) {
        //si language: nom du language, 
        //si skillLevel ou experience time: nom du skill
        String thingName;

        boolean selected = true;
        int n = getN();
        List<String> searchedThings = getSearchedThings();

        for (int i = 0; i < searchedThings.size(); i += 1) {
            thingName = searchedThings.get(i);
            if (filterName == "skillLevel" && a.getSkill(thingName) < n) {
                return false;
            } else if (filterName == "ExperienceTime" && a.getExperienceTime(thingName) < n) {
                return false;
            } else if (filterName == "languageLevel" && a.getLanguage(thingName) < n) {
                return false;
            }
        }
        return selected;
    }
}
