package fr.univ_lyon1.info.m1.cv_search.model.request;

import java.util.List;

import fr.univ_lyon1.info.m1.cv_search.model.Model;
import fr.univ_lyon1.info.m1.cv_search.model.applicant.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.applicant.ApplicantList;
import fr.univ_lyon1.info.m1.cv_search.model.applicant.ApplicantListBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.io.File;

public class Request extends ApplicantFilter {
    private ApplicantFilter[] appFilters;
    private String skillLevel;
    private String languageLevel;
    private String studyLevel;
    private String experienceTime;
    private int totalExpTime;
    private ApplicantFilter filtre;
    private ApplicantList listAllApplicants = new ApplicantListBuilder(new File(".")).build();
    private static Request request;
    
    private List<String> searchedSkills;
    private List<String> searchLanguages;
    private List<String> searchedThings; //searchedSkills ou searchLanguage

    private List<String> emptyList;

    /** Constructeur. */
    public Request() {

        appFilters = new ApplicantFilter[2];
        appFilters[0] = new FilterAllSuperior();
        appFilters[1] = new FilterOther();

        skillLevel = "";
        languageLevel = "";
        studyLevel = "";
        experienceTime = "";
        totalExpTime = 0;
        filtre = null;

        searchedSkills = null;
        searchLanguages = null;
        searchedThings = null;

        emptyList = new ArrayList<>();
    }

    /** Get singleton. */
    public static Request getInstance() {
        if (request == null) {
            request = new Request();
        }
        return request;
    }

    /** calls criteria filter(s).  */
    public ApplicantList filter(ApplicantList listApplicants, String filterName) {
        String[] strat = null;
        if (filterName == "totalExpTime") {
            filtre = appFilters[1]; //filterOther
            ((Filter) filtre).setFilterAttributes(totalExpTime, emptyList);
            return filtre.filter(listApplicants, filterName);
        }

        if (filterName == "studyLevel") {
            List<String> studyEquivalence = new ArrayList<>(Arrays.asList(
                "", "Bac", "Bac+1", "Bac+2", "Bac+3", "Bac+4", "Bac+5", "Bac+8"));
            int studyLevelInt = studyEquivalence.indexOf(studyLevel);
            filtre = appFilters[1]; //filterOther
            ((Filter) filtre).setFilterAttributes(studyLevelInt, emptyList);
            return filtre.filter(listApplicants, filterName);
        }

        if (filterName == "skillLevel") {
            strat = skillLevel.split(" ");  // par ex, "All >= 5" => ["All", ">=", "5"]
            searchedThings = searchedSkills;
        } else if (filterName == "ExperienceTime") {
            strat = experienceTime.split(" ");  // par ex, "All >= 5" => ["All", ">=", "5"]
            searchedThings = searchedSkills;
        } else if (filterName == "languageLevel") {
            List<String> languageEquivalence = new ArrayList<>(Arrays.asList(
                "", "A1", "A2", "B1", "B2", "C1", "C2")); //on met "" pour que l'indice min soit 1
            strat = languageLevel.split(" ");
            strat[2] = String.valueOf(languageEquivalence.indexOf(strat[2]));
            searchedThings = searchLanguages;
        }
        List<String> filterNames = Arrays.asList("All", "Average");
        int filterId = filterNames.indexOf(strat[0]); //0 si on a "All ...", 1 si "Avg ..."
        int n = Integer.parseInt(strat[2]); //le nombre n dans "X >= n"
        filtre = appFilters[filterId]; //on va soit vers le filtrerSuperior, soit vers filterOther
        ((Filter) filtre).setFilterAttributes(n, searchedThings);
        return filtre.filter(listApplicants, filterName);
    }

    /** Filter applicant using the given parameters 
     * and returns a list of applicant whose CVs were selected .*/
    public List<String> makeRequest(Model model, 
        List<String> searchedSkills, List<String> searchLanguages) {
            
        this.searchedSkills = searchedSkills;
        this.searchLanguages = searchLanguages;
        skillLevel = model.getSkillLevel();
        languageLevel = model.getLanguageLevel();
        studyLevel = model.getStudyLevel();
        experienceTime = model.getExpTimeRequired();
        totalExpTime = model.getTotalExperienceTime();

        ApplicantList selectedApplicants = listAllApplicants;
        
        if (skillLevel != "") {
            selectedApplicants = filter(selectedApplicants, "skillLevel");
        } else if (searchedSkills.size() > 0) {
            skillLevel = "All >= 0";
            selectedApplicants = filter(selectedApplicants, "skillLevel");
        }

        if (experienceTime != "") {
            selectedApplicants = filter(selectedApplicants, "ExperienceTime");
        }

        if (languageLevel != "") {
            selectedApplicants = filter(selectedApplicants, "languageLevel");
        } else if (searchLanguages.size() > 0) {
            languageLevel = "All >= A1";
            selectedApplicants = filter(selectedApplicants, "languageLevel");
        }

        if (studyLevel != "") {
            selectedApplicants = filter(selectedApplicants, "studyLevel");
        }
    
        selectedApplicants = filter(selectedApplicants, "totalExpTime");
        selectedApplicants.sortList(model.getSortBy());

        //ajouter la moyenne du applicant
        List<String> applicantsAndAverages = new ArrayList<String>();
        String strAverage;
        if (model.getShowSkillsAverage()) {
            for (Applicant a : selectedApplicants) {
                float average = a.calculateAverage(searchedSkills);
                if (average == 101) {
                    strAverage = ", skill average : None";
                } else {
                    strAverage = ", skill average : " + average; 
                }
                applicantsAndAverages.add(a.getName() + strAverage);
            }
        } else {
            for (Applicant a : selectedApplicants) {
                applicantsAndAverages.add(a.getName());
            }
        }
        return applicantsAndAverages;
    }

}
