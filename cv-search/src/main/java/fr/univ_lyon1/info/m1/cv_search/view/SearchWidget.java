package fr.univ_lyon1.info.m1.cv_search.view;


import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;
import fr.univ_lyon1.info.m1.cv_search.observer.ObserverRequest;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import java.util.List;
import java.util.ArrayList;

public class SearchWidget extends JfxViewComponent {
    private Button search = new Button("Search");
    private VBox resultBox;
    private SkillWidget skillWidget;
    private ComboBoxLanguage comboBoxLanguage;

    /** searchWidget constructor. */
    public SearchWidget(Model model, Controller c, 
        SkillWidget skillWidget, ComboBoxLanguage comboBoxLanguage) {

        super(model, c);
        this.skillWidget = skillWidget;
        this.comboBoxLanguage = comboBoxLanguage;
        setObserver(new ObserverRequest(model, c));
    }

    /**
     * Create the widget showing the list of applicants.
     */
    public Node createResultsWidget() {
        resultBox = new VBox();
        return resultBox;
    }

    /**
     * Create the widget used to trigger the search.
     */
    public Node createSearchWidget() {
        search.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                resultBox.getChildren().clear();

                //création de la liste des skills sélectionnés
                List<String> searchSkills = new ArrayList<>();
                for (Node skill : skillWidget.getSearchSkillBox().getChildren()) {
                    Label skillLabel = (Label) ((HBox) skill).getChildren().get(0);
                    String skillName = skillLabel.getText();
                    searchSkills.add(skillName);
                }

                //création de la liste des languages sélectionnés
                List<String> searchLanguages = new ArrayList<>();
                for (Node lang : comboBoxLanguage.getSearchSkillBox().getChildren()) {
                    Label languageLabel = (Label) ((HBox) lang).getChildren().get(0);
                    String languageName = languageLabel.getText();
                    searchLanguages.add(languageName);
                }

                List<String> selectedApplicants = 
                    getController().askSearchResults(getModel(), searchSkills, searchLanguages);
                notifyObservers(selectedApplicants);
            }      
        });
        return search;
    }

    /** Update the result. */
    public void updateResultBox() {
        resultBox.getChildren().clear();

        //création d'une liste des skills sélectionnés
        List<String> searchSkills = new ArrayList<>();
        for (Node skill : skillWidget.getSearchSkillBox().getChildren()) {
            Label skilllabel = (Label) ((HBox) skill).getChildren().get(0);
            String skillName = skilllabel.getText();
            searchSkills.add(skillName);
        }

        List<String> selectedApplicants 
            = getModel().getSelectedApplicants();
        
        for (int i = 0; i < selectedApplicants.size(); i++) {
            resultBox.getChildren().add(new Label(selectedApplicants.get(i)));
        }
    }
}
