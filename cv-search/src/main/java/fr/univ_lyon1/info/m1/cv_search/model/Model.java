package fr.univ_lyon1.info.m1.cv_search.model;

import java.util.List;
import java.util.ArrayList;

public class Model {

    private String skillLevel;
    private String expTimeRequired;
    private Boolean showSkillsAverage;
    private int totalExperienceTime;
    private List<String> skillList = new ArrayList<>();
    private List<String> selectedApplicants;
    private String sortBy;
    private String studyLevel;
    private String languageLevel;
    private List<String> languageList = new ArrayList<>();


    /** initialisation of model. */
    public void init() {
        this.setSkillLevel("");
        this.setExpTimeRequired("");
        this.changeLanguages("");
        this.setSortBy("alphabetical");
        this.setStudyLevel("");
        this.setShowSkillsAverage(false);
        this.setTotalExperienceTime(0);
        this.setLanguageLevel("");
    }

    /** setter SkillLevel. Call by Controller*/
    public void setSkillLevel(String skillLevel) {
        this.skillLevel = skillLevel;
    }

    /** setter for required experience time. Call by Controller*/
    public void setExpTimeRequired(String expTimeRequired) {
        this.expTimeRequired = expTimeRequired;
    }
    
    /** setter skillList. Call by Controller*/
    public String changeSkillList(String skillName) {
        if (skillList.contains(skillName)) {
            skillList.remove(skillName);
            return skillName;
        } else {
            skillList.add(skillName);
            return "";
        }
    }

    public void setStudyLevel(String studylevel) {
        this.studyLevel = studylevel;
    }

    /** setter showSkillAverage. Call by Controller*/
    public void setShowSkillsAverage(Boolean showSkillsAverage) {
        this.showSkillsAverage = showSkillsAverage;
    }

    /** setter totalExperienceTime. Call by Controller*/
    public void setTotalExperienceTime(int totalExperienceTime) {
        this.totalExperienceTime = totalExperienceTime;
    }

    /** setter sortBy. Call by Controller*/
    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    /** setter languageLevel. Call by Controller*/
    public void setLanguageLevel(String languageLevel) {
        this.languageLevel = languageLevel;
    }

    /** setter Request. Call by Controller*/
    public void setRequest(List<String> selectedApplicants) {
        this.selectedApplicants = selectedApplicants;
    }

    /** setter languageList. Call by Controller*/
    public String changeLanguages(String languageString) {
        if (languageList.contains(languageString)) {
            languageList.remove(languageString);
            return languageString;
        } else {
            languageList.add(languageString);
            return "";
        }
    }

    /** getter totalExperienceTime. Call by JfxView*/
    public int getTotalExperienceTime() {
        return totalExperienceTime;
    }

    /** getter skillList. Call by JfxView*/
    public List<String> getSkillList() {
        return skillList;
    }

    /** getter languageList. Call by JfxView*/
    public List<String> getLanguageList() {
        return languageList;
    }

    /** getter lest element of laguageList. Call by JfxView*/
    public String getLastLanguage() {
        return languageList.get(languageList.size() - 1);
    }

    /** getter lastSkill which is add. Call by JfxView*/
    public String getLastSkill() {
        return skillList.get(skillList.size() - 1);
    }

    /** getter SkillLevel name. Call by JfxView*/
    public String getSkillLevel() {
        return skillLevel;
    }

    /** getter for required experience time . Call by JfxView*/
    public String getExpTimeRequired() {
        return expTimeRequired;
    }

    /** getter showSkillAverage. Call by JfxView*/
    public Boolean getShowSkillsAverage() {
        return showSkillsAverage;
    }

    /** getter SelectedApplicants. Call by JfxView*/
    public List<String> getSelectedApplicants() {
        return selectedApplicants;
    }

    /** getter sortBy. Call by JfxView*/
    public String getSortBy() {
        return sortBy;
    }

    /** getter sortBy. Call by JfxView*/
    public String getStudyLevel() {
        return studyLevel;
    }

    /** getter languageLevel. Call by JfxView*/
    public String getLanguageLevel() {
        return languageLevel;
    }
}
