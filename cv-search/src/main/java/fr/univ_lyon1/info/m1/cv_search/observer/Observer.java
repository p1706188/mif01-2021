package fr.univ_lyon1.info.m1.cv_search.observer;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;

public abstract class Observer {
    private Model model;
    private Controller c;

    /** Constructor. */
    Observer(Model model, Controller c) {
        this.model = model;
        this.c = c;
    }

    /** update. */
    public abstract void update(Object object);

    /** Model getter. */
    Model getModel() {
        return model;
    }

    /** Controller getter. */
    Controller getController() {
        return c;
    }
}

