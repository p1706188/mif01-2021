package fr.univ_lyon1.info.m1.cv_search.view;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.Stage;


public class JfxView {
    private SkillWidget skillWidget;
    private ComboBoxLanguage comboBoxLanguage;
    private SearchWidget searchWidget;
    private Model model;
    private Controller controller;
    private ComponentComboBox comboBoxStudyLevel;
    private ComponentComboBox comboBoxSkillLevel;
    private ComponentComboBox comboBoxExpTime;
    private ComponentComboBox comboBoxLanguageLevel;
    private ComponentComboBox comboBoxSortBy;
    private ShowAverageButton showAverageButton;
    private TotalExperienceTimeBox totalExperienceTimeBox;
  


    /**
     * Create the main view of the application.
     */
    public JfxView(Controller c, Stage stage, int width, int height, Model m) {
        model = m;
        controller = c;
        c.addView(this); //on ajoute la vue à la liste de views du controleur
        
        stage.setTitle("Search for CV"); // Name of window

        VBox root = new VBox();
        root.setSpacing(15);

        HBox skillLanguageBox = addSkillsAndLanguages();
        root.getChildren().add(skillLanguageBox);


        //partie au milieu avec la box strategy à gauche, 
        //et le choix de total exp time et le tri à droite
        HBox stratExpTriBox = new HBox();
        stratExpTriBox.setAlignment(Pos.BASELINE_CENTER);

        
        VBox strategyBox = makeStrategyBox();
        VBox expTriBox = chooseTotalExpTimeAndTri();

        HBox.setHgrow(strategyBox, Priority.ALWAYS);
        HBox.setHgrow(expTriBox, Priority.ALWAYS);

        stratExpTriBox.getChildren().addAll(strategyBox, expTriBox);
        root.getChildren().add(stratExpTriBox);
        

        //bouton pour montrer ou non les moyennes des skills demandés dans les résultats
        showAverageButton = new ShowAverageButton(m, c);
        Node showAverageButtonBox = showAverageButton.createShowAverageButtonWidget();
        root.getChildren().add(showAverageButtonBox);


        //partie pour l'affichage des résulats après recherche
        VBox searchBox = new VBox();
        searchWidget = new SearchWidget(model, c, skillWidget, comboBoxLanguage);
        Node search = searchWidget.createSearchWidget();
        searchBox.getChildren().add(search);

        Node resultBox = searchWidget.createResultsWidget();
        searchBox.getChildren().add(resultBox);
        root.getChildren().add(searchBox);
        

        root.setPadding(new Insets(5, 5, 5, 5));

        // Everything's ready: add it to the scene and display it
        Scene scene = new Scene(root, width, height);
        stage.setScene(scene);
        stage.show();

        model.init();
    }  

    /** Section to add/delete skills and languages. */
    public HBox addSkillsAndLanguages() {
        VBox skillBox = new VBox();
        skillWidget = new SkillWidget(model, controller);
        Node newSkillBox = skillWidget.createNewSkillWidget();
        skillBox.getChildren().add(newSkillBox);
        
        Node searchSkillsContainer = skillWidget.createCurrentSearchSkillsWidget();
        skillBox.getChildren().add(searchSkillsContainer);
        skillBox.setStyle("-fx-border-color : black; -fx-border-width : 0 1 0 0; ");

        //partie pour ajouter / supprimer des langages
        VBox languageBox = new VBox();
        languageBox.setPadding(new Insets(0, 0, 0, 15));
        comboBoxLanguage = new ComboBoxLanguage(model, controller);
        Node comboBoxL = comboBoxLanguage.createComboBoxLanguage();
        languageBox.getChildren().add(comboBoxL);

        Node searchLanguagesContainer = comboBoxLanguage.createCurrentSearchLanguagesWidget();
        languageBox.getChildren().add(searchLanguagesContainer);


        HBox skillLanguageBox = new HBox();
        skillLanguageBox.getChildren().addAll(skillBox, languageBox);
        return skillLanguageBox;
    }


    /** Section to choose total experience time and how to sort by.  */
    public VBox chooseTotalExpTimeAndTri() {
        VBox expTriBox = new VBox();
        expTriBox.setAlignment(Pos.BASELINE_CENTER);
        expTriBox.setSpacing(10);

        //choix de l'experience totale (indépendante des skills)
        totalExperienceTimeBox = new TotalExperienceTimeBox(model, controller);
        Node totalExpTimeB = totalExperienceTimeBox.createTotalExperienceTimeBoxWidget();
        expTriBox.getChildren().add(totalExpTimeB);

        //comboBox pour le tri des resultats
        String infoStr = "Results sorted by applicant name if alphabetical, "
            + " by experience time if total / average experience time";
        comboBoxSortBy = new ComponentComboBox(model, controller, "Sort by");
        List<String> sortPossibilities = new ArrayList<String>(Arrays.asList(
            "alphabetical", "total experience time", "average experience time"));
        Node comboBoxSortB = comboBoxSortBy.createComboBox(sortPossibilities, infoStr);
        expTriBox.getChildren().add(comboBoxSortB);
        return expTriBox;
    }

    /** make StrategyBox.  */
    public VBox makeStrategyBox() {
        HBox strategyAndInfo = new HBox();
        Label strategyLabel = new Label("  Strategy");

        String infoStr = "CV choosing strategy. Choose criterias to add applicant expectations.";
        InfoCircle infoC = new InfoCircle(infoStr);
        StackPane info = infoC.getInfoCircle();   

        strategyAndInfo.getChildren().addAll(strategyLabel, info);

        VBox strategyContainer = new VBox();
        strategyContainer.setSpacing(20);
        strategyContainer.setStyle("-fx-border-color: grey;\n" 
                    + "-fx-border-insets: 5;\n" 
                    + "-fx-border-width: 1;\n");
        strategyContainer.setPadding(new Insets(0, 0, 15, 0));

        HBox strategyUp = new HBox();
        strategyUp.setAlignment(Pos.BASELINE_CENTER);

        HBox strategyDown = new HBox();
        strategyDown.setStyle("-fx-padding: 5;");

        
        //comboBox pour le skill level
        comboBoxSkillLevel = new ComponentComboBox(model, controller, "Skill level");
        List<String> skillLvlElements = new ArrayList<String>(Arrays.asList(
            "", "All >= 50", "All >= 60", "Average >= 50"));
        Node comboBoxSkillLvl = comboBoxSkillLevel.createComboBox(skillLvlElements, "");
        strategyUp.getChildren().add(comboBoxSkillLvl);

        //comboBox pour le niveau de langue
        comboBoxLanguageLevel = new ComponentComboBox(model, controller, "Language level");
        List<String> langLvlElements = new ArrayList<String>(Arrays.asList(
            "", "All >= A1", "All >= A2", "All >= B1", "All >= B2", "All >= C1", "All >= C2"));
        Node comboBoxLangLvl = comboBoxLanguageLevel.createComboBox(langLvlElements, "");
        strategyUp.getChildren().add(comboBoxLangLvl);

        //comboBox pour l'experience time spécifique aux skills
        comboBoxExpTime = new ComponentComboBox(model, controller, "Skill experience time");
        List<String> expTimeElements = new ArrayList<String>(Arrays.asList(
            "", "All >= 1", "All >= 3", "All >= 5", "All >= 10", "Average >= 10"));
        Node comboBoxExpT = comboBoxExpTime.createComboBox(expTimeElements, "");
        strategyDown.getChildren().add(comboBoxExpT);
        
        //comboBox pour le niveau d'étude
        comboBoxStudyLevel = new ComponentComboBox(model, controller, "Study level");
        List<String> studyLvlElements = new ArrayList<String>(Arrays.asList(
            "", "Bac", "Bac+1", "Bac+2", "Bac+3", "Bac+5", "Bac+8"));
        Node comboBoxStudyL = comboBoxStudyLevel.createComboBox(studyLvlElements, "");
        strategyDown.getChildren().add(comboBoxStudyL);


        HBox.setHgrow(comboBoxSkillLvl, Priority.ALWAYS);
        HBox.setHgrow(comboBoxExpT, Priority.ALWAYS);
        HBox.setHgrow(comboBoxStudyL, Priority.ALWAYS);
        HBox.setHgrow(comboBoxLangLvl, Priority.ALWAYS);

        strategyContainer.getChildren().addAll(strategyUp, strategyDown);
        
        VBox strategyBox = new VBox();
        strategyBox.getChildren().addAll(strategyAndInfo, strategyContainer);
        return strategyBox;
    }

    /** skillBox getter. */
    public SkillWidget getSkillBox() {
        return skillWidget;
    }

    /** comboBoxSkillLevel getter. */
    public ComponentComboBox getComboBoxSkillLevel() {
        return comboBoxSkillLevel;
    }

    /** comboBoxExpTime getter. */
    public ComponentComboBox getComboBoxExpTime() {
        return comboBoxExpTime;
    }

    /** comboBoxLanguage getter. */
    public ComboBoxLanguage getComboBoxLanguage() {
        return comboBoxLanguage;
    }

    /** comboBoxSortBy getter. */
    public ComponentComboBox getComboBoxSortBy() {
        return comboBoxSortBy;
    }

    /** comboBoxStudyLevel getter. */
    public ComponentComboBox getComboBoxStudyLevel() {
        return comboBoxStudyLevel;
    }

    /** comboBoxLanguageLevel getter. */
    public ComponentComboBox getComboBoxLanguageLevel() {
        return comboBoxLanguageLevel;
    }

    /** showAverageButton getter. */
    public ShowAverageButton getShowAverageButton() {
        return showAverageButton;
    }

    /** totalExperienceTimeBox getter. */
    public TotalExperienceTimeBox getTotalExperienceTimeBox() {
        return totalExperienceTimeBox;
    }

    /** searchWidget getter. */
    public SearchWidget getSearchWidget() {
        return searchWidget;
    }

    
}
