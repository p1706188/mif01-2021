package fr.univ_lyon1.info.m1.cv_search.observer;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;

public class ObserverSortBy extends Observer {
    /** constructor. */
    public ObserverSortBy(Model model, Controller c) {
        super(model, c);
    }

    /** surchage update. */
    public void update(Object sortBy) {
        getModel().setSortBy((String) sortBy);
        getController().updateView(7, "");
    }
}
