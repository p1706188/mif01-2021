package fr.univ_lyon1.info.m1.cv_search;

import java.util.ArrayList;
import java.util.List;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;
import fr.univ_lyon1.info.m1.cv_search.view.JfxView;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Main class for the application (structure imposed by JavaFX).
 */
public class App extends Application {

    /**
     * With javafx, start() is called when the application is launched.
     */
    @Override
    public void start(Stage stage) throws Exception {
        Controller c = new Controller();
        List<JfxView> viewList = new ArrayList<>();
        Model m = new Model();
        new JfxView(c, stage, 550, 600, m);
        new JfxView(c, new Stage(), 600, 600, m);
        c.addViews(viewList);
    }


    /**
     * A main method in case the user launches the application using
     * App as the main class.
     */
    public static void main(String[] args) {
        Application.launch(args);
    }
}
