package fr.univ_lyon1.info.m1.cv_search.model.applicant;

import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ApplicantList implements Iterable<Applicant> {
    private List<Applicant> list = new ArrayList<Applicant>();

    public void add(Applicant a) {
        list.add(a);
    }

    public void remove(Applicant a) {
        list.remove(a);
    }

    public int size() {
        return list.size();
    }

    @Override
    public Iterator<Applicant> iterator() {
        return list.iterator();
    }

    /** Clear the list of applicants. */
    public void clear() {
        list.clear();
    }

    /** Sets the content of the applicant list. */
    public void setList(ApplicantList list) {
        this.list = list.list;
    }


    /** faire une fonction pour trier selon average et selon le temps d'experience. */
    public void sortList(String sortBy) {
        List<Applicant> listSorted = new ArrayList<Applicant>();
        LinkedList<Applicant> listTmp = new LinkedList<Applicant>();
        for (int i = 0; i < list.size(); i++) {
            if (listTmp.size() == 0) {
                listTmp.add(list.get(i));
            } else {
                for (int j = 0; j < i; j++) {
                    if (list.get(i).compareTo(listTmp.get(j), sortBy) < 0) {
                        listTmp.add(j, list.get(i));
                        break;
                    } else {
                        if (j == i - 1) {
                            listTmp.add(list.get(i));
                        }
                    }
                }
            }
        }
        for (int i = 0; i < listTmp.size(); i++) {
            listSorted.add(listTmp.get(i));
        }
        this.list = listSorted;
    }
}
