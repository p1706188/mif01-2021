package fr.univ_lyon1.info.m1.cv_search.view;

import javafx.geometry.Insets;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;
import javafx.util.Duration;

public class InfoCircle {
    private StackPane infoCircle;
    
    /** InfoCircle constructor. */
    public InfoCircle(String string) {
        Text text = new Text("i");
        text.setBoundsType(TextBoundsType.VISUAL);

        Circle circleBorder = new Circle();
        circleBorder.setFill(Color.BLACK);
        circleBorder.setRadius(11);

        Circle circle = new Circle();
        circle.setFill(Color.WHITE);
        circle.setRadius(10);

        Tooltip tooltip = new Tooltip(string);
        tooltip.setShowDelay(Duration.seconds(1));
        tooltip.setWrapText(true);
        tooltip.setPrefWidth(250);

        Tooltip.install(circle, tooltip);

        StackPane stack = new StackPane();
        stack.getChildren().addAll(circleBorder, circle, text);
        stack.setPadding(new Insets(0, 5, 0, 5));
        infoCircle = stack;
    }

    /** InfoCircle getter. */
    public StackPane getInfoCircle() {
        return infoCircle;
    }
}
