package fr.univ_lyon1.info.m1.cv_search.observer;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;

import java.util.List;

public class ObserverRequest extends Observer {
    /** constructor. */
    public ObserverRequest(Model model, Controller c) {
        super(model, c);
    }
   
    /** surcharge update. */
    public void update(Object request) {
        @SuppressWarnings("unchecked")
        List<String> requestList = (List<String>) request;
        getModel().setRequest(requestList);
        getController().updateView(5, "");
    }
}
