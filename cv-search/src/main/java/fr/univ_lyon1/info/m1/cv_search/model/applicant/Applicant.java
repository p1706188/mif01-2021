package fr.univ_lyon1.info.m1.cv_search.model.applicant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

public class Applicant {
    private Map<String, Integer> skills = new HashMap<>();
    private Map<String, Integer> experienceTime = new HashMap<>();
    private Map<String, Integer> languages = new HashMap<>();
    private List<String> languageEquivalence = new ArrayList<String>(Arrays.asList(
        "", "A1", "A2", "B1", "B2", "C1", "C2"));
    private int studyLevel;
    private List<String> studyLevelEquivalence = new ArrayList<String>(Arrays.asList(
        "", "Bac", "Bac+1", "Bac+2", "Bac+3", "Bac+4", "Bac+5", "Bac+8"));
    private int totalExperienceTime = 0;
    private String name;

    public int getSkill(String string) {
        return skills.getOrDefault(string, -1);
    }

    public void setSkill(String string, int value) {
        skills.put(string, value);
    }

    public int getExperienceTime(String string) {
        return experienceTime.getOrDefault(string, 0);
    }

    public void setExperienceTime(String string, int value) {
        experienceTime.put(string, value);
    }

    public int getLanguage(String string) {
        return languages.getOrDefault(string, 0);
    }

    public void setLanguage(String language, String value) {
        languages.put(language, languageEquivalence.indexOf(value));
    }

    public int getStudyLevel() {
        return studyLevel;
    }

    public void setStudyLevel(String studyLevel) {
        this.studyLevel = studyLevelEquivalence.indexOf(studyLevel);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalExperienceTime() {
        return totalExperienceTime;
    }

    public void setTotalExperienceTime(int totalExperienceTime) {
        this.totalExperienceTime = totalExperienceTime;
    }

    /** returns i<0 if a.attribut < a2.attribut, i=0 if a.attribut  a2.attribut, i>0 else. */
    public int compareTo(Applicant a2, String sortBy) {
        switch (sortBy) {
            case "alphabetical" :
                return this.name.compareTo(a2.name);
            case "total experience time" :
                return this.totalExperienceTime - a2.totalExperienceTime;
            case "average experience time" :
                int av1;
                int av2;
                if (this.experienceTime.size() != 0) {
                    av1 = this.totalExperienceTime / this.experienceTime.size();
                } else {
                    av1 = 0;
                }
                
                if (a2.experienceTime.size() != 0) {
                    av2 = a2.totalExperienceTime / a2.experienceTime.size();
                } else {
                    av2 = 0;
                }
                
                return av1 - av2;
            default :
                return 0;
        }
    }

    /** calculates skills average. */
    public float calculateAverage(List<String> searchSkills) {
        float average = 101;
        int nbSkills = 0; 
        int addedSkills = 0;
        int searchSkillsSize = searchSkills.size(); 
        for (int i = 0; i < searchSkillsSize; i += 1) {
            String skillName = searchSkills.get(i);
            addedSkills += getSkill(skillName); //somme des niveaux
            nbSkills += 1; //nombre de skills qu'on évalue
        }
        if (nbSkills > 0) {
            average = addedSkills / nbSkills;
        }
        return average;
    } 

    /** renvoie le temps d'experience moyen. */
    public float calculateAverageTime(List<String> searchSkills) {
        float average = 101;
        float nbSkills = 0; 
        int addedTime = 0;
        int size = searchSkills.size(); 
        for (int i = 0; i < size; i += 1) {
            String skillName = searchSkills.get(i);
            addedTime += getExperienceTime(skillName); //somme des niveaux
            nbSkills += 1; //nombre de skills qu'on évalue
        }
        if (nbSkills > 0) {
            average = addedTime / nbSkills;
        }
        return average;
    }  
}
