package fr.univ_lyon1.info.m1.cv_search.observer;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;

public class ObserverShowAverage extends Observer {
    /** constructor. */
    public ObserverShowAverage(Model model, Controller c) {
        super(model, c);
    }
   
    /** surchage update. */
    public void update(Object showSkillsAverage) {
        getModel().setShowSkillsAverage((Boolean) showSkillsAverage);
        getController().updateView(3, "");
    }
}
