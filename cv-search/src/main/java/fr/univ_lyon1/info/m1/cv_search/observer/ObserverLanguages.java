package fr.univ_lyon1.info.m1.cv_search.observer;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;

public class ObserverLanguages extends Observer {
    /** constructor. */
    public ObserverLanguages(Model model, Controller c) {
        super(model, c);
    }

    /** surchage update. */
    public void update(Object languageString) {
        if (((String) languageString).contains("UPD")) {
            String str = ((String) languageString).replace("UPD", "");
            getController().updateView(11, str);
        } else {
            String str = getModel().changeLanguages((String) languageString);
            getController().updateView(8, str);
        } 
    }
}
