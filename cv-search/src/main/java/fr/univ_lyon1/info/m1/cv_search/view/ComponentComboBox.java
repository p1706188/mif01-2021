package fr.univ_lyon1.info.m1.cv_search.view;

import java.util.List;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;
import fr.univ_lyon1.info.m1.cv_search.observer.ObserverExpTime;
import fr.univ_lyon1.info.m1.cv_search.observer.ObserverLanguageLevel;
import fr.univ_lyon1.info.m1.cv_search.observer.ObserverSkillLevel;
import fr.univ_lyon1.info.m1.cv_search.observer.ObserverSortBy;
import fr.univ_lyon1.info.m1.cv_search.observer.ObserverStudyLevel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.geometry.Pos;


public class ComponentComboBox extends JfxViewComponent {
    private ComboBox<String> comboBox = new ComboBox<>();
    private String labelStr;

    /** Constructor. */
    public ComponentComboBox(Model model, Controller c, String labelStr) {
        super(model, c);
        this.labelStr = labelStr;
        if (labelStr == "Skill level") {
            setObserver(new ObserverSkillLevel(model, c));
        } else if (labelStr == "Study level") {
            setObserver(new ObserverStudyLevel(model, c));
        } else if (labelStr == "Skill experience time") {
            setObserver(new ObserverExpTime(model, c));
        } else if (labelStr == "Sort by") {
            setObserver(new ObserverSortBy(model, c));
        } else if (labelStr == "Language level") {
            setObserver(new ObserverLanguageLevel(model, c));
        }
        
    }

    /** création et renvoi de la comboBox . */
    public Node createComboBox(List<String> comboBoxElements, String infoStr) {
        for (int i = 0; i < comboBoxElements.size(); i++) {
            comboBox.getItems().addAll(comboBoxElements.get(i)); 
        }
        
        comboBox.setValue(comboBoxElements.get(0));

        EventHandler<ActionEvent> skillHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                notifyObservers(comboBox.getValue());
            }
        };

        comboBox.setOnAction(skillHandler);

        VBox box = new VBox();
        Label label = new Label(labelStr);
        if (infoStr == "") {
            box.getChildren().addAll(label, ((Node) comboBox));
        } else {
            HBox labelEtInfo = new HBox();
            labelEtInfo.setAlignment(Pos.BASELINE_CENTER);
            InfoCircle infoC = new InfoCircle(infoStr);
            StackPane info = infoC.getInfoCircle();
            labelEtInfo.getChildren().addAll(label, info);
            box.getChildren().addAll(labelEtInfo, comboBox);
        }

        box.setAlignment(Pos.BASELINE_CENTER);
        return box;
    }

    /** Update model. */
    public void updateModel() {
        Model model = getModel();
        if (labelStr == "Skill level") {
            comboBox.setValue(model.getSkillLevel());
        } else if (labelStr == "Study level") {
            comboBox.setValue(model.getStudyLevel());
        } else if (labelStr == "Skill experience time") {
            comboBox.setValue(model.getExpTimeRequired());
        } else if (labelStr == "Sort by") {
            comboBox.setValue(model.getSortBy());
        } else if (labelStr == "Language level") {
            comboBox.setValue(model.getLanguageLevel());
        }
    }
}
