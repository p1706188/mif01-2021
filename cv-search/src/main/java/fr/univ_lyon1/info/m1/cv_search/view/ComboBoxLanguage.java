package fr.univ_lyon1.info.m1.cv_search.view;

import java.util.List;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;
import fr.univ_lyon1.info.m1.cv_search.observer.ObserverLanguages;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.geometry.Insets;
import javafx.geometry.Pos;


public class ComboBoxLanguage extends JfxViewComponent {
    private ComboBox<String> comboBoxLanguage = new ComboBox<>();
    private FlowPane searchLanguagesBox = new FlowPane();

    /** Constructor. */
    public ComboBoxLanguage(Model model, Controller c) {
        super(model, c);
        setObserver(new ObserverLanguages(model, c));
    }

    /** creation et renvoie de la comboBox pour les langages. */
    public Node createComboBoxLanguage() {
        createComboBox();
        EventHandler<ActionEvent> languageHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String text = comboBoxLanguage.getValue();
                if (text.equals("")) {
                    return; // Do nothing
                }
                List<String> languageList = getModel().getLanguageList();
                if (!languageList.contains(text)) { 
                    //enters if user tries to add a language which is already added
                    notifyObservers(text);
                }
            }
        };

        HBox languageBoxDown = new HBox();
        Button submitButton = new Button("Add language");
        submitButton.setOnAction(languageHandler);

        languageBoxDown.getChildren().addAll(comboBoxLanguage, submitButton);
        languageBoxDown.setSpacing(5);
        languageBoxDown.setAlignment(Pos.BASELINE_LEFT);

        VBox languageBox = new VBox();
        languageBox.setSpacing(3);
        Label labelLanguage = new Label("Languages");
        languageBox.getChildren().addAll(labelLanguage, languageBoxDown);

        HBox.setHgrow(languageBox, Priority.ALWAYS);

        return languageBox;
    }

    /** Create the comboBox. */
    public void createComboBox() {
        comboBoxLanguage.getItems().addAll(
            "",
            "English",
            "French",
            "Spanish",
            "German",
            "Japanese",
            "Chinese"
        );
        comboBoxLanguage.setValue("");
        EventHandler<ActionEvent> languagesHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String str = comboBoxLanguage.getValue();
                str = str + "UPD";
                notifyObservers(str);
            }
        };
        comboBoxLanguage.setOnAction(languagesHandler);
    }

    public void updateSelectedLanguage(String selectedLanguage) {
        comboBoxLanguage.setValue(selectedLanguage);
    }

    /** Update skills. */
    public void updateLanguages() {    
        final HBox box = new HBox();
        String text = getModel().getLastLanguage();
        comboBoxLanguage.setValue(text);
        Label languageLabel = new Label(text);
        languageLabel.setPadding(new Insets(0, 5, 0, 5));
        Button b = new Button("x");
        b.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                notifyObservers(text);
            }
            
        });
        box.setStyle("-fx-padding: 2;" + "-fx-border-style: solid inside;"
                        + "-fx-border-width: 1;" + "-fx-border-insets: 5;"
                        + "-fx-border-radius: 5;" + "-fx-border-color: black;");
        box.setAlignment(Pos.BASELINE_CENTER);
        box.getChildren().addAll(languageLabel, b);
        searchLanguagesBox.getChildren().add(box);
    }

    /** Update strategy. */
    public void updateRemoveLanguages(String languageString) {
        Node nodeRemove = null;
        for (Node language : searchLanguagesBox.getChildren()) {
            Label languagelabel = (Label) ((HBox) language).getChildren().get(0);
            String languageNameBis = languagelabel.getText();
            if (languageString == languageNameBis) {
                nodeRemove = language;
                break;
            }
        }
        if (nodeRemove != null) {
            searchLanguagesBox.getChildren().remove(nodeRemove);
        }
    }

    public Node createCurrentSearchLanguagesWidget() {
        return searchLanguagesBox;
    }

    /** searchLanguageBox getter. */
    public FlowPane getSearchSkillBox() {
        return searchLanguagesBox;
    }
}
