package fr.univ_lyon1.info.m1.cv_search.observer;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;

public class ObserverLanguageLevel extends Observer {
    /** constructor. */
    public ObserverLanguageLevel(Model model, Controller c) {
        super(model, c);
    }

    /** surcharge update. */
    public void update(Object skillLanguageLevel) {
        getModel().setLanguageLevel((String) skillLanguageLevel);
        getController().updateView(9, "");
    }
}
