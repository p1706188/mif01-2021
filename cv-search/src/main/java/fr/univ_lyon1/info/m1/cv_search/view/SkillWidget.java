package fr.univ_lyon1.info.m1.cv_search.view;

import java.util.List;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;
import fr.univ_lyon1.info.m1.cv_search.observer.ObserverSkill;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

public class SkillWidget extends JfxViewComponent {
    private FlowPane searchSkillsContainer = new FlowPane();
    
    /** Constructor. */
    public SkillWidget(Model model, Controller c) {
        super(model, c);
        setObserver(new ObserverSkill(model, c));
    }

    /** searchSkillsBox getter. */
    public FlowPane getSearchSkillBox() {
        return searchSkillsContainer;
    }

    /**
     * Create the text field to enter a new skill.
     */
    public Node createNewSkillWidget() {
        HBox skillBoxDown = new HBox();
        TextField textField = new TextField();
        Button submitButton = new Button("Add skill");
        skillBoxDown.getChildren().addAll(textField, submitButton);
        skillBoxDown.setSpacing(10);
        skillBoxDown.setAlignment(Pos.BASELINE_LEFT);

        VBox skillBox = new VBox();
        skillBox.setSpacing(3);
        Label labelSkill = new Label("Skill");
        skillBox.getChildren().addAll(labelSkill, skillBoxDown);
        
        EventHandler<ActionEvent> skillHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String text = textField.getText().strip();
                if (text.equals("")) {
                    return; // Do nothing
                }
                List<String> skillList = getModel().getSkillList();
                if (!skillList.contains(text)) { //if user try to add a skill which is already add
                    notifyObservers(text);
                } 
                    

                textField.setText("");
                textField.requestFocus();
            }
        };
        submitButton.setOnAction(skillHandler);
        textField.setOnAction(skillHandler);

        HBox.setHgrow(skillBox, Priority.ALWAYS);

        return skillBox;
    }

    /** Update skills. */
    public void updateSkills() {    
        final HBox box = new HBox();
        String text = getModel().getLastSkill();
        Label skillLabel = new Label(text);
        skillLabel.setPadding(new Insets(5, 5, 0, 5));

        Button b = new Button("x");
        b.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                notifyObservers(text);
            }
        });

        box.setStyle("-fx-padding: 2;" + "-fx-border-style: solid inside;"
                        + "-fx-border-width: 1;" + "-fx-border-insets: 5;"
                        + "-fx-border-radius: 5;" + "-fx-border-color: black;");
        box.setAlignment(Pos.BASELINE_CENTER);
        box.getChildren().addAll(skillLabel, b);
        searchSkillsContainer.getChildren().add(box);
    }

    /** Delete skills. */
    public void updateRemoveSkill(String skillName) {
        Node nodeRemove = null;
        for (Node skill : searchSkillsContainer.getChildren()) {
            Label skilllabel = (Label) ((HBox) skill).getChildren().get(0);
            String skillNameBis = (skilllabel).getText();
            if (skillName == skillNameBis) {
                nodeRemove = skill;
                break;
            }
        }
        if (nodeRemove != null) {
            searchSkillsContainer.getChildren().remove(nodeRemove);
        }
    }

    /**
     * Create the widget showing the list of skills currently searched
     * for.
     */
    public Node createCurrentSearchSkillsWidget() {
        return searchSkillsContainer;
    }
}
