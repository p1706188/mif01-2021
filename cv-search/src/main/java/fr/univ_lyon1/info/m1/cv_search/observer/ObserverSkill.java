package fr.univ_lyon1.info.m1.cv_search.observer;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;

public class ObserverSkill extends Observer {
    /** constructor. */
    public ObserverSkill(Model model, Controller c) {
        super(model, c);
    }

    /** surchage update. */
    public void update(Object skillName) {
        String str = getModel().changeSkillList((String) skillName);
        getController().updateView(1, str);
    }
}
