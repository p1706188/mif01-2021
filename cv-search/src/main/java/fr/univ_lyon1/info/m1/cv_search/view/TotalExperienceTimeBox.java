package fr.univ_lyon1.info.m1.cv_search.view;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;
import fr.univ_lyon1.info.m1.cv_search.observer.ObserverTotalExpTime;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.geometry.Pos;


public class TotalExperienceTimeBox extends JfxViewComponent {
    private VBox totalExpTimeBox = new VBox();
    private SpinnerValueFactory<Integer> valueFactory = 
                new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 50, 0);

    /** Constructor. */
    public TotalExperienceTimeBox(Model model, Controller c) {
        super(model, c);
        setObserver(new ObserverTotalExpTime(model, c));
    }

    /** Create the widget showing the spinner for the experience time required (in years). */
    public Node createTotalExperienceTimeBoxWidget() {
        HBox labelEtInfo = new HBox();
        labelEtInfo.setAlignment(Pos.BASELINE_CENTER);

        Label label = new Label("Total experience time");
        String infoStr = "Applicant's total experience time, no matter the job." 
            + "This experience time is independant from skill, " 
            + "unlike the strategy box's experience time element.";
        InfoCircle infoC = new InfoCircle(infoStr);
        StackPane info = infoC.getInfoCircle();  
        labelEtInfo.getChildren().addAll(label, info);


        Spinner<Integer> spinner = new Spinner<Integer>();
        spinner.setValueFactory(valueFactory);

        spinner.valueProperty().addListener((obs, oldValue, newValue) -> {
            notifyObservers(newValue);
        });  

        totalExpTimeBox.getChildren().addAll(labelEtInfo, spinner);
        totalExpTimeBox.setAlignment(Pos.BASELINE_CENTER);

        return totalExpTimeBox;
    }


    /** Update the value of TotalExperienceTime. */
    public void updateTotExpTime() {
        valueFactory.setValue(getModel().getTotalExperienceTime());
        
    }
}
