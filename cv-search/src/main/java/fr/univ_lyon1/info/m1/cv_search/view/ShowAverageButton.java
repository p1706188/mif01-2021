package fr.univ_lyon1.info.m1.cv_search.view;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;
import fr.univ_lyon1.info.m1.cv_search.observer.ObserverShowAverage;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.HBox;
import javafx.geometry.Pos;

public class ShowAverageButton extends JfxViewComponent {
    private CheckBox showAverageButton = new CheckBox("Show skill average");
    
    /** Constructor. */
    public ShowAverageButton(Model model, Controller c) {
        super(model, c);
        setObserver(new ObserverShowAverage(model, c));
    }


    /**Create the widget showing the checkbox for the skill average. */
    public Node createShowAverageButtonWidget() {
        HBox showAverageBox = new HBox();

        EventHandler<ActionEvent> skillHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                boolean isSelected = showAverageButton.isSelected();
                notifyObservers(isSelected);
            }
        };
        showAverageButton.setOnAction(skillHandler);

        showAverageBox.getChildren().addAll(showAverageButton);
        showAverageBox.setAlignment(Pos.BOTTOM_LEFT);
        return showAverageBox;
    }

    /** Update the button check shox average. */
    public void  updateShowAverage() {
        showAverageButton.setSelected(getModel().getShowSkillsAverage());
    }
    
}
