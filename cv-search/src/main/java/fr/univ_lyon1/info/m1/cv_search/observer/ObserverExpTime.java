package fr.univ_lyon1.info.m1.cv_search.observer;

import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import fr.univ_lyon1.info.m1.cv_search.model.Model;

public class ObserverExpTime extends Observer {
    /** constructor. */
    public ObserverExpTime(Model model, Controller c) {
        super(model, c);
    }

    /** surcharge update. */
    public void update(Object expTimeRequired) {
        getModel().setExpTimeRequired((String) expTimeRequired);
        getController().updateView(4, "");
    }
}


