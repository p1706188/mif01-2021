# CV SEARCH
## Objectifs du projet 
Ce projet a pour but de mettre en place un outil de sélection de cv afin 
de permettre à un recruteur de filtrer des candidats en fonction de critères 
préalablement renseignés. Le projet est codé en Java. Nous avons notamment
 utilisé la bibliothèque d’interface utilisateur Javafx pour le frontend de l’application.

Auteurs : Cécile Zagala et Malaurie Gomez

## Installation 
Tout d'abord, il faut vérifier que maven et que git soient bien installés. 
Si ce n'est pas le cas, lancez : 

    apt install git maven

Il faut ensuite cloner notre projet, puis ce rendre dans le bon répertoire : 

    git clone https://forge.univ-lyon1.fr/p1706188/mif01-2021.git
    cd mif01-2021/cv-search/

Compilez et lancez le projet :

    mvn compile
    mvn exec:java


## Remarques 
Les diagrammes UML ont été faits majoritairement avec l'outil umbrello seulement
 le diagramme de séquence a été fait avec App Creately (nous n'avons pas réussi à créer un fichier natif,
 une capture d'écran remplacera ce diagramme).
